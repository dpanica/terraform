

#we will use this to define the number of needed servers

variable "prefix" {
  default = "pncdana-terraform-production"
}
variable "vpc_cidr_block" {
  default = "10.10.0.0/16"
}

