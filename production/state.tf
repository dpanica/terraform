

#terraform state, config S3

terraform {
  backend "s3" {
    bucket         = "terraform-production-workshop"
    encrypt        = true
    key            = "terraform-dana-prodcution.tfstate"
    region         = "eu-north-1"
    dynamodb_table = "lock-tf"
  }
}

